Role Name
=========

This role have utilities to be used when performing tests with Ansible, in order to facilitate validation of configuration.

Requirements
------------

This role does not have any additional requirement

Role Variables
--------------

This role does not have any variable required and should be used as a library to help you in your tests.

Dependencies
------------

This role does not have any dependency

Example Usage
-------------

This role is not intended to be used at playbook level but inside tasks of your tests.
All this tasks are expected to be iterable with the named vars, so both 'loop_control' is mandatory.

This role can be used to test:

### **User**

This task 'test_user.yml' is used as main entrypoint to test user exist.
Mandatory structure of the task calling it MUST BE:

    - name: "Test user"
      include_role:
        name: "test_utilities"
        tasks_from: "test-user"
      loop:
        - "{{ user_to_be_tested_list }}"
      loop_control:
        loop_var: expected

Format of the {{ user_to_be_tested_list }} have register for each user that you want to list.
Example testing user `root` is:

    - name: "Test user"
      include_role:
        name: "test_utilities"
        tasks_from: "test-user"
      loop:
        - "root"
      loop_control:
        loop_var: expected

### **Group**

This task 'test_group.yml' is used as main entrypoint to test group exist.
Mandatory structure of the task calling it MUST BE:

    - name: "Test group"
      include_role:
        name: "test_utilities"
        tasks_from: "test-group"
      loop:
        - "{{ group_to_be_tested_list }}"
      loop_control:
        loop_var: expected

Format of the {{ group_to_be_tested_list }} have register for each group that you want to list.
Example testing group `root` is:

    - name: "Test group"
      include_role:
        name: "test_utilities"
        tasks_from: "test-group"
      loop:
        - "root"
      loop_control:
        loop_var: expected

### **Package**

This tasks 'test_pkg.yml' is used as main entrypoint to test if packages are installed.
Mandatory structure of the task calling it MUST BE:

    - name: "Test packages"
      include_role:
        name: "test_utilities"
        tasks_from: "test-package"
      loop:
        - "{{ packages_to_be_tested_dictionary }}"
      loop_control:
        loop_var: expected

Format of the {{ packages_to_be_tested_dictionary }} items have the following keys:

    | Parameter | Choices/default | Description                                                               |
    |-----------|-----------------|---------------------------------------------------------------------------|
    | name      |                 | name of the package. Not used for the check but for identify the package. |
    | version   |                 | package and version to be installed                                       |

Example testing package `tomcat`is:

    - name: "Test packages"
      include_role:
        name: "test_utilities"
        tasks_from: "test-package"
      loop:
        - name: "Tomcat package"
          version: "tomcat-7.0.1"
      loop_control:
        loop_var: expected

### **Directory**

This task 'test_dir.yml' are used as main entrypoint to test directories path, user, group and mode.
Mandatory structure of the task calling it MUST BE:

    - name: "Test directories"
      include_role:
        name: "test_utilities"
        tasks_from: "test-directory"
      loop:
        - path: "{{ directories_to_be_tested_dictionary }}"
      loop_control:
        loop_var: expected

Format of the {{ directories_to_be_tested_dictionary }} items have the following keys:

    | Parameter | Choices/default | Description                       |
    |-----------|-----------------|-----------------------------------|
    | path      |                 | directory path to check if exists |
    | owner     | Default: "root" | owner of the directory            |
    | group     | Default: "root" | group of the directory            |
    | mode      | Default: "0755" | permissions of the directory      |

Example testing directory `/etc` is:

    - name: "Test directories"
      include_role:
        name: "test_utilities"
        tasks_from: "test-directory"
      loop:
        - path: "/etc"
          owner: "root"
          group: "root"
          mode: "0755"
      loop_control:
        loop_var: expected

### **Files**

This tasks 'test_file.yml' are used as main entrypoint to test files path, user, group, mode but also the content if you want to check lines, yml, json or xml.

    - name: "Test files"
      include_role:
        name: "test_utilities"
        tasks_from: "test-file"
      loop:
        - "{{ files_to_be_tested_dictionary }}"
      loop_control:
        loop_var: expected

Format of the {{ files_to_be_tested_dictionary }} items have the following keys:

    | Parameter    | Required                        | Choices/default                   | Description                                                                                                                                                                                                                                                                                                                                                |
    |--------------|---------------------------------|-----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | path         | yes                             |                                   | File path to be tested                                                                                                                                                                                                                                                                                                                                     |
    | owner        | no                              | Default: "root"                   | User of the file                                                                                                                                                                                                                                                                                                                                           |
    | group        | no                              | Default: "root"                   | Group of the file                                                                                                                                                                                                                                                                                                                                          |
    | mode         | no                              | Default: "0644"                   | Permissions of the file                                                                                                                                                                                                                                                                                                                                    |
    | content_type | no                              | Choices: ["lines", "yml", "json"] | Content to check. Need content parameter too.  * lines: list parameter with lines to check * yml: dictionary parameter with keys "path" and "expected" * json: dictionary parameter with keys "path" and "expected"  Watch content                                                                                                                         |
    | content      | only if content_type is defined |                                   | If content_type is lines, this parameter is a list with expected lines  If content_type is yml, json this parameter is a dictionary with actual and expected keys.  The actual keys are the path to the expected key value. For yaml example to test "hola":  file:   value: "hola"  Content parameter will be:  - actual: "file.value"   expected: "hola" |

Example testing files content:

    - name: "Test files"
      include_role:
        name: "test_utilities"
        tasks_from: "test-file"
      loop:
        - path: "/etc/file/path"
          owner: "user"
          group: "usergroup"
          mode: "0644"
          content_type: "lines"
          content:
            - "this-is-the-expected-line={{ variable }}"

        - path: "/etc/file/path.yml"
          owner: "user"
          group: "usergroup"
          mode: "0644"
          content_type: "yml"
          content:
            - actual: "yaml.path.key"
              expected: "value of thee yaml.path.key"

        - path: "/etc/file/path.json"
          owner: "user"
          group: "usergroup"
          mode: "0644"
          content_type: "json"
          content:
            - actual: "json.path.key"
              expected: "value of thee json.path.key"
      loop_control:
        loop_var: expected

### **Link**

This task 'test-link.yml' is used as main entrypoint to test group exist.
Mandatory structure of the task calling it MUST BE:

    - name: "Test link"
      include_role:
        name: "test_utilities"
        tasks_from: "test-link"
      loop:
        - "{{ link_to_be_tested_list }}"
      loop_control:
        loop_var: expected

Format of the {{ link_to_be_tested_list }} have register for each link that you want to list.
Example testing link `/usr/bin/python3` is:

    - name: "Test group"
      include_role:
        name: "test_utilities"
        tasks_from: "test-link"
      loop:
        - "/usr/bin/python3"
      loop_control:
        loop_var: expected


License
-------

BSD

Author Information
------------------

Juan Antonio
